"""Welcome to Reflex! This file outlines the steps to create a basic app."""
import reflex as rx

def index():
        return rx.text('Hello, world!', font_size='200px',
        color='blue', bg='yellow', as_='b', _hover={'color':'white', 'bg':'black'})



def about():
        return rx.text('About Page ........')

app = rx.App()
app.add_page(index)
app.add_page(about, route='/about')
app.compile()