import reflex as rx

class BiscuitConfig(rx.Config):
    pass

config = BiscuitConfig(
    app_name="biscuit",
)