FROM python:3.9-slim

WORKDIR /app
COPY . .

RUN apt update -y
RUN apt install unzip curl -y 
RUN pip install reflex==0.3.4
RUN reflex init
CMD ["reflex", "run"]
